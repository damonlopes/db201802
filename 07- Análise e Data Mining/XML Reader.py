"""
Equipe FDT:
- Francois Bruno Rueckert, 1367200, francois.bruno
- Damon Nicholas Lopes Oliniski, 1367161, damonlopes
- Tiago Colli Plakitca, 1795490, xstriker
"""

from xml.dom import minidom
from xml.dom.minidom import parse
import xml.dom.minidom
import urllib2
import psycopg2
import psycopg2.extras

try:
    conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
except:
    print "I am unable to connect to the database."

cur = conn.cursor()

"""------------------- OPEN MOVIES DOC ---------------------------"""
Movies = xml.dom.minidom.parse("movies.xml").documentElement
movieList = Movies.getElementsByTagName('Movie') 

movieUri = [items.attributes['movieUri'].value for items in movieList]
movieTitle = [items.attributes['title'].value for items in movieList]
movieYear = [items.attributes['year'].value for items in movieList]
movieGenre = [items.attributes['genre'].value for items in movieList]

"""------------------- OPEN MOVIES DOC ---------------------------"""
Music = xml.dom.minidom.parse("music.xml").documentElement
musicList = Music.getElementsByTagName('Music');

musicUri = [items.attributes['musicUri'].value for items in musicList]
musicTitle = [items.attributes['name'].value for items in musicList]
musicGenre = [items.attributes['genre'].value for items in musicList]
musicOrigin = [items.attributes['origin'].value for items in musicList]

try:
    cur.execute("DELETE FROM FILME")
    cur.execute("DELETE FROM ARTISTA_MUSICAL")
except Exception as e:
    print "I can't delete!"
    print e

for x in range(len(movieList)):
    print "movieUri: %s" % movieUri[x][33:]
    print "movieTitle: %s" % movieTitle[x]
    print "movieYear: %s" % movieYear[x]
    print "movieGenre: %s" % movieGenre[x]
    try:
        cur.execute("INSERT INTO FILME(id_filme,nome,data_lancamento,categoria)VALUES(%s,%s,%s,%s)", 
                    (movieUri[x][33:],movieTitle[x],movieYear[x],movieGenre[x]))
    except Exception as e:
        print "I can't insert!"
        print e
    conn.commit()
    
for y in range(len(musicList)):
    print "musicUri: %s" % musicUri[x][33:]
    print "musicTitle: %s" % musicTitle[x]
    print "musicGenre: %s" % musicGenre[x]
    print "musicOrigin: %s" % musicOrigin[x]
    try:
        cur.execute("INSERT INTO ARTISTA_MUSICAL(id_artista,pais,genero,nome_artistico)VALUES(%s,%s,%s,%s)", 
                    (musicUri[x][33:],musicOrigin[x],musicGenre[x],musicTitle[x]))
    except Exception as e:
        print "I can't insert!"
        print e
    conn.commit()
    
