"""
Equipe FDT:
- Francois Bruno Rueckert, 1367200, francois.bruno
- Damon Nicholas Lopes Oliniski, 1367161, damonlopes
- Tiago Colli Plakitca, 1795490, xstriker
"""

import sys
from imdb import IMDb
import psycopg2
import urllib.request
import tmdbsimple as tmdb
from bs4 import BeautifulSoup

try:
    conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
except:
    print("I am unable to connect to the database.")

filmes_id = conn.cursor()
artistas_id = conn.cursor()

filmes_id.execute("SELECT id_filme FROM filme ORDER BY id_filme")
artistas_id.execute("SELECT id_artista FROM artista_musical ORDER BY id_artista")

ia = IMDb()
tmdb.API_KEY = 'ce0fae6eede7fd814552889ab7170697'
search = tmdb.Search()
ofile1 = (open('movies.xml',"w"))
ofile1.write("<AllMovies>")
filmes = filmes_id.fetchall()
filmeAnterior = "nenhum"
for filme in filmes :
	if filmeAnterior != filme:
		filmeId = filme[0]
		strfilme=filme[0].replace("http://www.imdb.com/title/tt","")
		movie=ia.get_movie(strfilme[:-1])
		Titulo = movie['title']

		pesqTmdb = search.movie(query = '%s' % Titulo)
		movieTmdb = tmdb.Movies(pesqTmdb['results'][0]['id'])
		response = movieTmdb.info()
		budget = movieTmdb.budget
		revenue = movieTmdb.revenue

		Nota = movie['rating']
		AnoLanc = movie['year']
		Diretor = movie['director']
		dir=Diretor[0]
		Atores=movie['actors']
		filmeGen=movie['genre']
		actor1=Atores[0]
		actor2=Atores[1]
		actor3=Atores[2]
		ofile1.write('\n<Movie movieUri="%s" title="%s" rating="%s" year="%s" genre="%s" director="%s" actor1="%s" actor2="%s" actor3="%s" budget="%d" revenue="%d"/>'%(filmeId,Titulo,Nota,AnoLanc,filmeGen[0],dir,actor1,actor2,actor3,budget,revenue)) #Itens extras: Nota IMDb,os três atores principais, Orçamento e Receita do filme
		print("Filme: " + Titulo)
	filmeAnterior = filme
ofile1.write("\n</AllMovies>")

ofile2 = (open('music.xml',"w"))
ofile2.write("<AllMusic>")
urlAnterior="nenhum"
todasasurl = artistas_id.fetchall()
for url in todasasurl:
	if url[0]!=urlAnterior:
		try:
			page = urllib.request.urlopen(url[0])
		except :
			page = ""
			print("Essa url esta em utf-8")
		try:
			soup = BeautifulSoup(page,features = "lxml")
		except :
		 	soup = BeautifulSoup(page,"html")
		origem_art = ""
		genero_art = ""
		anos_ativo_artista = ""

		tabelacerta=soup.find('table',{"class" : 'infobox vcard plainlist'})
		print ("------------------------------------------------------------------")
		print ("url:" + url[0])
		if tabelacerta != None:
			nome = 0
			nome = soup.h1.string
			for row in tabelacerta.find_all('tr'):
				if row.th != None and row.td != None and row.th.string == "Origin" :
					origem_art=""
					origem = row.td.find_all(text=True)
					for ori in origem:
						origem_art = origem_art + ori
				elif row.th != None and row.td != None and row.th.string == "Born":
					origem = row.td.find_all(text=True)
					for ori in origem:
						origem_art = origem_art + ori
					try:
						origem_art = origem_art.split('\n')[1]
					except:
						origem_art = ""
				elif row.th != None and row.td != None and row.th.string == "Genres" :
					genero = row.td.find_all(text=True)
					for gen in genero:
						genero_art = genero_art + gen
					genero_art = genero_art.replace('\n\n\n','').replace('\n',',')
				elif row.th != None and row.td != None and row.th.string == "Years active" :
					anos_ativos = row.td.find_all(text=True)
					for anos in anos_ativos:
						anos_ativo_artista = anos_ativo_artista + anos
					anos_ativo_artista = anos_ativo_artista.replace('\n\n\n','').replace('\n',',')

			print ("Nome: " + nome)
			print ("Origem: " + origem_art)
			print ("Anos de atividade: " + anos_ativo_artista)
			print ("Genero: " + genero_art)

			ofile2.write('\n<Music musicUri="%s" name="%s" origin="%s" year_active="%s" genre="%s"/>'% (url[0],nome.replace('&','e'),origem_art.replace('&','e'),anos_ativo_artista.replace('&','e'),genero_art.replace('&','e'))) #Item extra : Ano de atividade do artista/banda

		else:
			print ("Nome: ")
			print ("Origem: ")
			print ("Anos de atividade: ")
			print ("Genero: ")

	urlAnterior = url[0]
ofile2.write("\n</AllMusic>")

artistas_id.close()
filmes_id.close()
conn.close()
sys.exit(0)
