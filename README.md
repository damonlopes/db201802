# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

Modifique este aquivo README.md com as informações adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usuário GitLab para cada integrante do grupo.

- François Bruno Rueckert, 1367200, francois.bruno
- Damon Nicholas Lopes Oliniski, 1367161, damonlopes
- Tiago Colli Plakitca, 1795490, xstriker

## Descrição da aplicação a ser desenvolvida 

- O que vocês vão implementar:
Uma clusterização para definir grupos dentro da rede social e colocar novos membros dentro de seus respectivos grupos de acordo com os interesses musicais, filmes e amizades com outras pessoas. A partir dos grupos criados faremos indicações de pessoas, filmes e músicos para os novos usuários. As indicações serão os elementos mais populares de cada grupo que ainda não foram curtidos/adicionados pelo usuário.
________________________________________________________________________________________________________________________________________________________
	
- Quais tecnologias vão utilizar:
As bibliotecas de python: pandas, NumPy, matplotlib, scikit-learn.
________________________________________________________________________________________________________________________________________________________

- Por que decidiram pelo tema:
Por ser um tema interessante, recente e por pertencer ao segmento de data mining e machine learning, que se encaixam bem na disciplina e projeto requisitado.
________________________________________________________________________________________________________________________________________________________

 - Quais os desafios relacionados com bancos de dados (consultas, modelos, etc.)
Escolher os dados a serem utilizados para a clusterização de forma que os grupos sejam bem definidos e realizar as consultas para a obtenção dos dados necessários.
________________________________________________________________________________________________________________________________________________________

- Quais os desafios relacionados com suas áreas de interesse:
 Aprender a usar os algoritmos e entender como as redes neurais não supervisionadas funcionam para criar os clusters.
Escolher os dados da forma correta para a obtenção de um resultado melhor e uma precisão maior na hora de criar os grupos.
Entender o conceito de cluster e como isso se faz útil na aplicação em questão.
________________________________________________________________________________________________________________________________________________________
	
- Quais conhecimentos novos vocês pretendem adquirir:
Quais bibliotecas de phyton utilizar e como manipula-las para a obtenção do resultado desejado.
Entender como funcionam os algoritmos de clusterização.
________________________________________________________________________________________________________________________________________________________

- O que vocês já produziram (protótipos, esquemas de telas, teste de codificação de partes críticas, etc.)
Nada.
________________________________________________________________________________________________________________________________________________________
	
Nome: Tiago Colli Plakitca

- Qual o seu foco na implementação do trabalho?
Utilizar e conhecer mais sobre as bibliotecas de data mining do phyton e entender como são utilizados e para que servem os algoritmos de clusterização.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
Já tive contato com redes não supervisionadas na disciplina de Sistemas Inteligentes e elas são a base de como a clusterização funciona.

- Qual aspecto do trabalho te interessa mais?
Entender como através de padrões "aleatórios" uma rede neural pode extrair informações úteis e agrupar dados de forma precisa e acima de tudo útil para a aplicação em questão.

	
Nome: François Bruno Rueckert

- Qual o seu foco na implementação do trabalho?
Implementação e supervisão do funcionamento dos algoritmos escolhidos no restante do sistema

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
Não.

- Qual aspecto do trabalho te interessa mais?
Como criar e aprimorar os algoritmos que proporcionam o aprendizado a máquina de forma eficiente.


Nome: Damon Nicholas Lopes Oliniski
	
- Qual o seu foco na implementação do trabalho?
Coleta das informações necessárias para a otimização do algoritmo.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?
Não.

- Qual aspecto do trabalho te interessa mais?
Entender de forma clara como que funciona uma rede neural	
	
