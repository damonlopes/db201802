import psycopg2
import psycopg2.extras
from os import system, name

# import sleep to show output for some time period
from time import sleep

def clear():
    if name == 'posix':
        _ = system('clear')

def listar():
    clear()

    try:
        conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
    except:
        print "I am unable to connect to the database."

    cur = conn.cursor()

    try:
        cur.execute("SELECT login,nome_completo,cidade_natal FROM PESSOA")
    except Exception as e:
        print "I can't select"
        print e
    rows = cur.fetchall()
    print "Login ||| Nome ||| Cidade natal"
    for x in range(len(rows)):
        print rows[x][0] + " ||| " + rows[x][1] + " ||| " + rows[x][2]

def menuprincipal():
    clear()
    on = int(raw_input ("\tMENU PRINCIPAL\n\t1 - CADASTRO DE PESSOAS\n\t2 - LISTAGEM DE PESSOAS\n\t3 - SAIR\n\n\tO QUE DESEJA? "))
    return on

def menucadastro():
    clear()

    try:
        conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
    except:
        print "I am unable to connect to the database."

    cur = conn.cursor()

    uri = raw_input ("\tCADASTRO DE PESSOA\n\n\tINSIRA O LOGIN: ")
    name = raw_input ("\tINSIRA O NOME COMPLETO: ")
    hometown = raw_input ("\tINSIRA A CIDADE NATAL: ")

    try:
        cur.execute("INSERT INTO PESSOA(login,nome_completo,cidade_natal)VALUES(%s,%s,%s)", (uri,name,hometown))
    except Exception as e:
        print "I can't insert!"
        print e
    conn.commit()

    clear()
    addmusic = raw_input ("\tCLASSIFICACAO DE BANDA\n\n\tGOSTARIA DE CLASSIFICAR UMA BANDA?(y/n) ")
    while addmusic == 'y':
        bandUri = raw_input ("\tURI (WIKIPEDIA) DA BANDA: ")
        rating = raw_input ("\tCLASSIFICACAO (1-5) DA BANDA: ")
        clear()
        addmusic = raw_input ("\tCLASSIFICACAO DE BANDA\n\n\tGOSTARIA DE CLASSIFICAR OUTRA BANDA?(y/n) ")

        try:
            cur.execute("INSERT INTO ARTISTA_MUSICAL(id_artista)VALUES(%s)",(bandUri,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO CURTE_ARTISTA(id_pessoa,id_artista)VALUES(%s,%s)", (uri,bandUri,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO AVALIA_ARTISTA(id_pessoa,id_artista,nota)VALUES(%s,%s,%s)", (uri,bandUri,rating,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()

    clear()
    addmovie = raw_input ("\tCLASSIFICACAO DE FILME\n\n\tGOSTARIA DE CLASSIFICAR UM FILME?(y/n) ")
    while addmovie == 'y':
        movieUri = raw_input ("\tURI (IMDB) DO FILME: ")
        rating = raw_input ("\tCLASSIFICACAO (1-5) DO FILME: ")
        clear()
        addmovie = raw_input ("\tCLASSIFICACAO DE FILME\n\n\tGOSTARIA DE CLASSIFICAR OUTRO FILME?(y/n) ")

        try:
            cur.execute("INSERT INTO FILME(id_filme)VALUES(%s)", (movieUri,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO CURTE_FILME(login_pessoa,id_filme)VALUES(%s,%s)", (uri,movieUri,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO AVALIA_FILME(login_pessoa,id_filme,nota)VALUES(%s,%s,%s)", (uri,movieUri,rating,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()

    clear()
    addcoll = raw_input ("\tADICIONAR CONHECIDOS\n\n\tGOSTARIA DE ADICIONAR UM CONHECIDO?(y/n) ")
    clear()
    while addcoll == 'y':
        listar()
        colleague = raw_input ("\tLOGIN DO CONHECIDO: ")
        addcoll = raw_input ("\tGOSTARIA DE ADICIONAR OUTRO CONHECIDO?(y/n) ")
        clear()

        try:
            cur.execute("INSERT INTO CONHECE(login_pessoa1,login_pessoa2)VALUES(%s,%s)", (uri,colleague,))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()

    clear()
    raw_input("\tCADASTRO CONCLUIDO COM SUCESSO! PRESSIONE ENTER PARA VOLTAR")
    return

def menulistar():
    on = int(raw_input ("\n\tMENU DE LISTAGEM\n\t1 - APAGAR PESSOA\n\t2 - EDITAR PESSOA\n\t3 - VOLTAR\n\n\tO QUE DESEJA? "))
    return on

def menuapagar():#precisa arrumar as tabelas pra funcionar
    try:
        conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
    except:
        print "I am unable to connect to the database."

    cur = conn.cursor()

    name = raw_input ("\tLOGIN DE PESSOA A SER APAGADA: ")

    try:
        cur.execute("DELETE FROM AVALIA_ARTISTA WHERE  id_pessoa = %s", (name,))
        cur.execute("DELETE FROM CURTE_ARTISTA WHERE  id_pessoa = %s", (name,))
        cur.execute("DELETE FROM AVALIA_FILME WHERE  login_pessoa = %s", (name,))
        cur.execute("DELETE FROM CURTE_FILME WHERE  login_pessoa = %s", (name,))
        cur.execute("DELETE FROM CONHECE WHERE login_pessoa1 = %s", (name,))
        cur.execute("DELETE FROM CONHECE WHERE login_pessoa2 = %s", (name,))
        cur.execute("DELETE FROM PESSOA WHERE login = %s", (name,))
    except Exception as e:
        print "I can't delete!"
        print e
    conn.commit()
    raw_input("USUARIO EXCLUIDO COM SUCESSO! PRESSIONE ENTER PARA VOLTAR")#linha pra ver erros
    return

def menueditar():#precisa arrumar as tabelas pra funcionar e poder ser feito
    try:
        conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
    except:
        print "I am unable to connect to the database."

    cur = conn.cursor()
    name = raw_input("LOGIN DO USUARIO A SER EDITADO: ")
    editname = raw_input("DESEJA ALTERAR O NOME DO USUARIO?(y/n) ")
    if editname == 'y':
        newname = raw_input("NOVO NOME DO USUARIO: ")
        try:
            cur.execute("UPDATE PESSOA SET nome_completo = %s WHERE login = %s", (newname,name))
        except Exception as e:
            print "I can't delete!"
            print e
        conn.commit()
    edithometown = raw_input("DESEJA ALTERAR A CIDADE NATAL DO USUARIO?(y/n) ")
    if edithometown == 'y':
        newhometown = raw_input("NOVA CIDADE NATAL DO USUARIO: ")
        try:
            cur.execute("UPDATE PESSOA SET nome_completo = %s, cidade_natal = %s WHERE login = %s", (newname,newhometown,name))
        except Exception as e:
            print "I can't delete!"
            print e
        conn.commit()
    raw_input("USUARIO ALTERADO COM SUCESSO! PRESSIONE ENTER PARA VOLTAR")
    return

on = 0
while(not on):
    on = menuprincipal()
    if on == 1:
        menucadastro()
        on = 0
    if on == 2:
        listar()
        on = menulistar()
        if on == 1:
            menuapagar()
        if on == 2:
            menueditar()
        on = 0
    if on == 3:
        clear()
        print "\t\tPROGRAMA TERMINADO"
