"""
Equipe FDT:
- Francois Bruno Rueckert, 1367200, francois.bruno
- Damon Nicholas Lopes Oliniski, 1367161, damonlopes
- Tiago Colli Plakitca, 1795490, xstriker
"""

from xml.dom import minidom
from xml.dom.minidom import parse
import xml.dom.minidom
import urllib2
import psycopg2
import psycopg2.extras

try:
    conn = psycopg2.connect("dbname='1802FDT' user='1802FDT' host='200.134.10.32' password='398643'")
except:
    print "I am unable to connect to the database."

cur = conn.cursor()

url = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml' # Persons location
dom = minidom.parse(urllib2.urlopen(url)) # parsing data

Persons = dom.getElementsByTagName('Person')

uri = [items.attributes['uri'].value for items in Persons]
#for x in uri:
#  print "Uri: %s" % x

name = [items.attributes['name'].value for items in Persons]
#for x in name:
#  print "Name: %s" % x

hometown = [items.attributes['hometown'].value for items in Persons]
#for x in hometown:
#  print "Hometown: %s" % x

birthdate = [items.attributes['birthdate'].value for items in Persons]
#for x in birthdate:
#  print "Birthdate: %s" % x

url = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml' # Musics location
dom = minidom.parse(urllib2.urlopen(url)) # parsing data

Musics = dom.getElementsByTagName('LikesMusic')

url = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml' # Movies location
dom = minidom.parse(urllib2.urlopen(url)) # parsing data

Movies = dom.getElementsByTagName('LikesMovie')

url = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml' # Colleagues location
dom = minidom.parse(urllib2.urlopen(url)) # parsing data

Colleagues = dom.getElementsByTagName('Knows')
try:
    cur.execute("DELETE FROM CONHECE")
    cur.execute("DELETE FROM CURTE_FILME")
    cur.execute("DELETE FROM AVALIA_FILME")
    cur.execute("DELETE FROM FILME")
    cur.execute("DELETE FROM CURTE_ARTISTA")
    cur.execute("DELETE FROM AVALIA_ARTISTA")
    cur.execute("DELETE FROM ARTISTA_MUSICAL")
    cur.execute("DELETE FROM PESSOA")
except Exception as e:
    print "I can't delete!"
    print e

for x in range(len(uri)):
    print "Uri: %s" % uri[x][33:]
    print "Name: %s" % name[x]
    print "Hometown: %s" % hometown[x]
    print "Birthdate: %s" % birthdate[x]
    try:
        cur.execute("INSERT INTO PESSOA(login,nome_completo,cidade_natal)VALUES(%s,%s,%s)", (uri[x][33:],name[x],hometown[x]))
    except Exception as e:
        print "I can't insert!"
        print e
    conn.commit()
    print "Musics: "
    rating = [items.attributes['rating'].value for items in Musics if items.attributes['person'].value == uri[x]]
    bandUri = [items.attributes['bandUri'].value for items in Musics if items.attributes['person'].value == uri[x]]
    for y in range(len(bandUri)):
        try:
            cur.execute("INSERT INTO ARTISTA_MUSICAL(id_artista)VALUES(%s)",(bandUri[y],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO CURTE_ARTISTA(id_pessoa,id_artista)VALUES(%s,%s)", (uri[x][33:],bandUri[y],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO AVALIA_ARTISTA(id_pessoa,id_artista,nota)VALUES(%s,%s,%s)", (uri[x][33:],bandUri[y],rating[y],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        print "Rating: %s" % rating[y]
        print "Band: %s" % bandUri[y]
    print "Movies: "
    rating = [items.attributes['rating'].value for items in Movies if items.attributes['person'].value == uri[x]]
    movieUri = [items.attributes['movieUri'].value for items in Movies if items.attributes['person'].value == uri[x]]
    for y in range(len(movieUri)):
        #Bloco comentado porque nao esta funcionando, e para evitar erros no resto do programa
        try:
            cur.execute("INSERT INTO FILME(id_filme)VALUES(%s)", (movieUri[y],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO CURTE_FILME(login_pessoa,id_filme)VALUES(%s,%s)", (uri[x][33:],movieUri[y],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        try:
            cur.execute("INSERT INTO AVALIA_FILME(login_pessoa,id_filme,nota)VALUES(%s,%s,%s)", (uri[x][33:],movieUri[y],rating[y],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()
        print "Rating: %s" % rating[y]
        print "Movie: %s" % movieUri[y]
for x in range(len(uri)):
    print "Colleagues of " + uri[x][33:] + ":\n"
    colleague = [items.attributes['colleague'].value for items in Colleagues if items.attributes['person'].value == uri[x]]
    for y in range(len(colleague)):
        #Bloco comentado porque nao esta funcionando, e para evitar erros no resto do programa
        try:
            cur.execute("INSERT INTO CONHECE(login_pessoa1,login_pessoa2)VALUES(%s,%s)", (uri[x][33:],colleague[y][33:],))
        except Exception as e:
            print "I can't insert!"
            print e
        conn.commit()

        print "Knows: %s" % colleague[y]
    print "\n"
