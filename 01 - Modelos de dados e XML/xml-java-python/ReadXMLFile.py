"""
Equipe FDT:
- Francois Bruno Rueckert, 1367200, francois.bruno
- Damon Nicholas Lopes Oliniski, 1367161, damonlopes
- Tiago Colli Plakitca, 1795490, xstriker
"""
#!/usr/bin/python
import csv
import os
from xml.dom.minidom import parse
import xml.dom.minidom

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")

universe = DOMTree.documentElement

# Calculations variables
good = 0
bad = 0
total = 0
avgweight = 0
HulkBMI = 0

if universe.hasAttribute("name"):
   print "Root element : %s" % universe.getAttribute("name")

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")

# Check and create directory/files
if (os.path.exists("./dadosMarvel") == False):
    os.mkdir("./dadosMarvel")
if (os.path.exists("./dadosMarvel/herois.csv") == True):
    os.remove("./dadosMarvel/herois.csv")
allHeroes = open("./dadosMarvel/herois.csv","w+")
if (os.path.exists("./dadosMarvel/herois_good.csv") == True):
    os.remove("./dadosMarvel/herois_good.csv")
goodHeroes = open("./dadosMarvel/herois_good.csv","w+")
if (os.path.exists("./dadosMarvel/herois_bad.csv") == True):
    os.remove("./dadosMarvel/herois_bad.csv")
badHeroes = open("./dadosMarvel/herois_bad.csv","w+")

# Get detail of each hero.
for hero in heroes:
    """
    print "*****Hero*****"
    if hero.hasAttribute("id"):
       print "Id: %s" % hero.getAttribute("id")
    """
    name = hero.getElementsByTagName('name')[0]
    popularity = hero.getElementsByTagName('popularity')[0]
    align = hero.getElementsByTagName('alignment')[0]
    gender = hero.getElementsByTagName('gender')[0]
    height = hero.getElementsByTagName('height_m')[0]
    weight = hero.getElementsByTagName('weight_kg')[0]
    home = hero.getElementsByTagName('hometown')[0]
    intel = hero.getElementsByTagName('intelligence')[0]
    strength = hero.getElementsByTagName('strength')[0]
    speed = hero.getElementsByTagName('speed')[0]
    durability = hero.getElementsByTagName('durability')[0]
    energy = hero.getElementsByTagName('energy_Projection')[0]
    fight = hero.getElementsByTagName('fighting_Skills')[0]

    # Write data on herois.csv

    heroData = [[name.childNodes[0].data,popularity.childNodes[0].data,align.childNodes[0].data,gender.childNodes[0].data,height.childNodes[0].data,weight.childNodes[0].data,home.childNodes[0].data,intel.childNodes[0].data,strength.childNodes[0].data,speed.childNodes[0].data,durability.childNodes[0].data,energy.childNodes[0].data,fight.childNodes[0].data]]
    writer = csv.writer(allHeroes)
    writer.writerows(heroData)
    """
    print "Name: %s" % name.childNodes[0].data
    print "Popularity %s" % popularity.childNodes[0].data
    print "Alignment: %s" % align.childNodes[0].data
    print "Gender: %s" % gender.childNodes[0].data
    print "Height(m): %s" % height.childNodes[0].data
    print "Weight(kg): %s" % weight.childNodes[0].data
    print "Hometown: %s" % home.childNodes[0].data
    print "Intelligence: %s" % intel.childNodes[0].data
    print "Strength: %s" % strength.childNodes[0].data
    print "Speed: %s" % speed.childNodes[0].data
    print "Durability: %s" % durability.childNodes[0].data
    print "Energy Projection: %s" % energy.childNodes[0].data
    print "Fighting Skills: %s" % fight.childNodes[0].data
    """
    # Calculations and writing of the good and the bad heroes
    if align.childNodes[0].data == "Good":
        good=good+1.0
        writer = csv.writer(goodHeroes)
        writer.writerows(heroData)

    elif align.childNodes[0].data == "Bad":
        bad=bad+1.0
        writer = csv.writer(badHeroes)
        writer.writerows(heroData)

    if name.childNodes[0].data == "Hulk":
        HulkBMI=float(weight.childNodes[0].data)/(float(height.childNodes[0].data))**2

    avgweight=avgweight+float(weight.childNodes[0].data)
    total=total+1.0

# Print of the calculations
print "\nProportion of good and bad heroes: " + str(good/bad)
print "\nAvarage Hero's Weight: " + str(avgweight/total) + "kg"
print "\nHulk's Body Mass Index: " + str(HulkBMI) + "kg/m^2"
